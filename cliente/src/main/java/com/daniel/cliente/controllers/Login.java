package com.daniel.cliente.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Login {

    @Autowired
    private RestTemplate template;

    @GetMapping
    @RequestMapping("/login/{user}/{password}")
    public String getFoosBySimplePathWithPathVariable(
            @PathVariable("user") String user, @PathVariable("password") String password) {
        return template.getForObject("http://AUTH-SERVICE/login/" + user + "/" + password, String.class);
    }

    @GetMapping("/test")
    public String test() {
        return "working....";
    }

    @Bean
    @LoadBalanced
    public RestTemplate getTemplate() {
        return new RestTemplate();
    }
}
