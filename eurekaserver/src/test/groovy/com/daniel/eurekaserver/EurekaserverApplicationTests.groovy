package com.daniel.eurekaserver

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner)
@SpringBootTest
class EurekaserverApplicationTests {

	@Test
	void contextLoads() {
	}

}
